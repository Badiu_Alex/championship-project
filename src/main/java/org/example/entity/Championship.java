package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "championship")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Championship {

    @Id
    @GeneratedValue
    @Column(name = "championship_id")
    private Integer id;

    @Column(name = "championship_name")
    private String name;

    @OneToMany(mappedBy = "championship")
    List<Team> teams;

    public Championship(String name) {
        this.name = name;
    }
}
