package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "player")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {
    @Id
    @GeneratedValue
    @Column(name = "player_id")
    private Integer id;

    @Column(name = "player_name")
    private String name;

    @Column(name = "player_market_value")
    private Integer marketValue;

    @Column(name = "player_nationality")
    private String nationality;

    @ManyToMany
    @JoinTable(name="Enrollment",joinColumns = {@JoinColumn(name="player_id")},inverseJoinColumns = {@JoinColumn(name="team_id")})

    private List<Team> teams;

    public Player(String name, Integer marketValue, String nationality) {
        this.name = name;
        this.marketValue = marketValue;
        this.nationality = nationality;
    }
}
