package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="team")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {

    @Id
    @GeneratedValue
    @Column(name="team_id")
    private Integer id;

    @JoinColumn(name="team_championship_id",referencedColumnName = "championship_id")
    @ManyToOne
    private  Championship championship;

    @Column(name="team_name")
    private String name;

    @Column(name="team_club")
    private String club;

    public Team(String name, String club) {
        this.name = name;
        this.club = club;
    }
}
