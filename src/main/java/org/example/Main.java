package org.example;

import org.example.entity.Championship;
import org.example.entity.Player;
import org.example.entity.Team;
import org.example.repository.ChampionshipRepository;
import org.example.repository.PlayerRepository;
import org.example.repository.TeamRepository;
import org.example.utility.HibernateUtil;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args){

        Player player1 = new Player("A",70,"ENG");
        Player player2 = new Player("B",60,"FRA");
        Player player3 = new Player("C",80,"GER");
        Player player4 = new Player("D",90,"DAN");
        Player player5 = new Player("E",50,"IRL");

        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();

        PlayerRepository playerRepository1=new PlayerRepository(sessionFactory.createEntityManager());

        List<Player> playersToBeAdded = Arrays.asList(player1,player2,player3,player4);

        playerRepository1.saveAllPlayers(playersToBeAdded);
        playerRepository1.savePlayer(player5);

        System.out.println(playerRepository1.findAllPlayers());

        System.out.println(playerRepository1.getMaxMarketValue());

        System.out.println(playerRepository1.getAllPlayersNames());

        System.out.println(playerRepository1.deletePlayer(player3));

        System.out.println(playerRepository1.findAllPlayers());


        Championship championship1 = new Championship("Premier League");

        Team team1 = new Team("Anglia","Liverpool");
        Team team2 = new Team("Germania","Bayern");

        team1.setChampionship(championship1);
        team2.setChampionship(championship1);

        ChampionshipRepository championshipRepository= new ChampionshipRepository(sessionFactory.createEntityManager());
        championshipRepository.saveChampionship(championship1);


        TeamRepository teamRepository = new TeamRepository(sessionFactory.createEntityManager());
        teamRepository.saveTeam(team1);
        teamRepository.saveTeam(team2);


        player1.setTeams(Arrays.asList(team1,team2));
        player2.setTeams(Arrays.asList(team2));
        player4.setTeams(Arrays.asList(team1,team2));
        player5.setTeams(Arrays.asList(team1));

        List<Player> playersToBeAddedInTeams= Arrays.asList(player1,player2,player4,player5);

        playerRepository1.saveAllPlayers(playersToBeAddedInTeams);

    }
}