package org.example.utility;

import org.example.entity.Championship;
import org.example.entity.Team;
import org.example.entity.Player;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    public static SessionFactory getSessionFactory() {
        final SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Championship.class)
                .addAnnotatedClass(Team.class)
                .addAnnotatedClass(Player.class)
                .buildSessionFactory();
        return sessionFactory;
    }
}