package org.example.utility;

import org.example.entity.Player;

public class PlayerParser {
    public static Player fromCSV(String line){
        String[] data=line.split(",");
        return new Player(data [0],Integer.parseInt(data[1]),data[2]);
    }

    public static String toCSV(Player player){
        StringBuilder sb=new StringBuilder();
        sb.append(player.getName());
        sb.append(",");
        sb.append(player.getMarketValue());
        sb.append(",");
        sb.append(player.getNationality());
        sb.append("\n");
        return sb.toString();
    }
}
