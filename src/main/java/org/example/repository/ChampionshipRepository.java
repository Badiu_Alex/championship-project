package org.example.repository;

import org.example.entity.Championship;
import org.example.entity.Team;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class ChampionshipRepository {
    private EntityManager entityManager;

    public ChampionshipRepository(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public Championship saveChampionship (Championship championship) {
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.persist(championship);
            entityTransaction.commit();
            return championship;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public List<Championship> saveAllChampionships(List<Championship> championshipsToBeSaved){
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for(Championship championship:championshipsToBeSaved){
                entityManager.persist(championship);
            }
            entityTransaction.commit();
            return championshipsToBeSaved;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public Championship deleteChampionship (Championship championship) {
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.remove(championship);
            entityTransaction.commit();
            return championship;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public List<Championship> findAllChampionships(){
        return entityManager.createQuery("FROM championship", Championship.class).getResultList();
    }
}