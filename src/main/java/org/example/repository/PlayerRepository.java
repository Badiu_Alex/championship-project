package org.example.repository;

import org.example.entity.Player;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class PlayerRepository {
    private EntityManager entityManager;

    public PlayerRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Player savePlayer(Player player) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(player);
            entityTransaction.commit();
            return player;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Player> saveAllPlayers(List<Player> playersToBeSaved) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            for (Player player : playersToBeSaved) {
                entityManager.persist(player);
            }
            entityTransaction.commit();
            return playersToBeSaved;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public Player deletePlayer(Player player) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(player);
            entityTransaction.commit();
            return player;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Player> findAllPlayers() {
        return entityManager.createQuery("FROM player", Player.class).getResultList();
    }

    public List<String> getAllPlayersNames() {
        return entityManager.createQuery("SELECT name FROM player", String.class).getResultList();
    }

    public Integer getMaxMarketValue() {
        return (Integer) entityManager.createQuery("SELECT MAX(marketValue) FROM player").getSingleResult();

    }

    public Integer getSecondMaxMarketValue() {
        return (Integer) entityManager.createQuery("SELECT MAX(marketValue) FROM player WHERE marketValue < (SELECT MAX(marketValue) FROM player)").getSingleResult();
    }

    public Integer getThirdMaxMarketValue() {
        return (Integer) entityManager.createQuery("SELECT MAX(marketValue) FROM player WHERE marketValue < (SELECT MAX(marketValue) FROM player WHERE marketValue < (SELECT MAX(marketValue) FROM player))").getSingleResult();
    }

//    Query query = session.createQuery("from Employee e where e.idEmployee=:id");
//    query.setParameter("id",id);


}
