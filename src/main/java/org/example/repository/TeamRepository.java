package org.example.repository;

import org.example.entity.Team;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class TeamRepository {
    private EntityManager entityManager;

    public TeamRepository(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public Team saveTeam (Team team) {
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.persist(team);
            entityTransaction.commit();
            return team;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public List<Team> saveAllTeams(List<Team> teamsToBeSaved){
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            for(Team team:teamsToBeSaved){
                entityManager.persist(team);
            }
            entityTransaction.commit();
            return teamsToBeSaved;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public Team deleteTeam (Team team) {
        try{
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if(!entityTransaction.isActive()){
                entityTransaction.begin();
            }
            entityManager.remove(team);
            entityTransaction.commit();
            return team;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public List<Team> findAllTeams(){
        return entityManager.createQuery("FROM team", Team.class).getResultList();
    }

}
