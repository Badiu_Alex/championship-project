package org.example;

import org.example.entity.Player;
import org.example.repository.PlayerRepository;
import org.example.utility.HibernateUtil;
import org.example.utility.PlayerParser;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {

        Path filePath = Paths.get("export.csv");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        PlayerRepository playerRepository = new PlayerRepository(sessionFactory.createEntityManager());

        List<Player> players = playerRepository.findAllPlayers();

        for (Player player : players) {
            String p = PlayerParser.toCSV(player);
            try {
                Files.writeString(filePath, p, StandardOpenOption.APPEND);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }
}


