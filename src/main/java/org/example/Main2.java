package org.example;

import org.example.entity.Player;
import org.example.repository.PlayerRepository;
import org.example.utility.HibernateUtil;
import org.example.utility.PlayerParser;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main2 {
    public static void main(String[] args) {
        Path filePath = Paths.get("C:\\Users\\Alex\\Desktop\\JavaAdvanced\\championship_project\\src\\main\\resources\\players.csv");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        PlayerRepository playerRepository = new PlayerRepository(sessionFactory.createEntityManager());
        try {
            List<String> lines = Files.readAllLines(filePath);
            for (String line : lines) {
                Player player = PlayerParser.fromCSV(line);
                playerRepository.savePlayer(player);
            }
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

}
